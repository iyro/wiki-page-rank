package cs535.pagerank.comparators;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class RankSortingComparator extends WritableComparator {
    protected RankSortingComparator() {
        super(FloatWritable.class, true);
    }

    @Override
    public int compare(WritableComparable w1, WritableComparable w2) {
        FloatWritable k1 = (FloatWritable) w1;
        FloatWritable k2 = (FloatWritable) w2;

        return k2.compareTo(k1);
    }
}
