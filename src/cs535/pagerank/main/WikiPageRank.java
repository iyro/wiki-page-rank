package cs535.pagerank.main;

import cs535.pagerank.comparators.RankSortingComparator;
import cs535.pagerank.mappers.*;
import cs535.pagerank.reducers.*;
import cs535.pagerank.utils.XmlInputFormat;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counters;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.TaskCounter;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class WikiPageRank extends Configured implements Tool {

    private static final NumberFormat FORMAT = new DecimalFormat("00");
    private static int NumberOfIterations = 1;
    private static Long totalNumberOfPages = 0l;

    public static void main(String[] args) throws Exception {
        System.exit(ToolRunner.run(new Configuration(), new WikiPageRank(), args));
    }

    @Override
    public int run(String[] args) throws Exception {
        String inPath = args[1];
        String outPath = args[2] + "00";
        if (args.length == 4) {
            NumberOfIterations = Integer.parseInt(args[3]);
        }

        boolean isDone = runXmlParsing(inPath, outPath);
        if (!isDone) return 1;

        for (int runs = 0; runs < NumberOfIterations; runs++) {
            inPath = outPath;
            outPath = args[2] + FORMAT.format(runs + 1);

            isDone = runRankCalculation(inPath, outPath, runs);
            if (!isDone) return 1;

            deleteIfExistsAndGetPath(inPath);
        }

        isDone = runIdealRankSorting(outPath, args[2]);
        if (!isDone) return 1;

        isDone = runTaxedRankSorting(outPath, args[2]);
        if (!isDone) return 1;

        isDone = runDifferenceCalculation(outPath, args[2]);
        if (!isDone) return 1;

        deleteIfExistsAndGetPath(outPath);
        return 0;
    }

    private Path deleteIfExistsAndGetPath(String file) throws IOException {
        FileSystem fs = FileSystem.get(getConf());
        Path p = new Path(file);
        if (fs.exists(p)) fs.delete(p, true);
        return p;
    }

    private boolean runXmlParsing(String inputPath, String outputPath) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();
        conf.set(XmlInputFormat.START_TAG_KEY, "<page>");
        conf.set(XmlInputFormat.END_TAG_KEY, "</page>");

        Job xmlParser = Job.getInstance(conf, "xmlParser");
        xmlParser.setJarByClass(WikiPageRank.class);

        FileInputFormat.addInputPath(xmlParser, new Path(inputPath));
        FileOutputFormat.setOutputPath(xmlParser, deleteIfExistsAndGetPath(outputPath));

        xmlParser.setInputFormatClass(XmlInputFormat.class);
        xmlParser.setOutputFormatClass(TextOutputFormat.class);

        xmlParser.setMapperClass(PageParsingMapper.class);
        xmlParser.setMapOutputKeyClass(Text.class);

        xmlParser.setReducerClass(PageParsingReducer.class);

        xmlParser.setOutputKeyClass(Text.class);
        xmlParser.setOutputValueClass(Text.class);

        if (xmlParser.waitForCompletion(true)) {
            Counters counters = xmlParser.getCounters();
            totalNumberOfPages = counters.findCounter(TaskCounter.REDUCE_OUTPUT_RECORDS).getValue();
            return true;
        } else return false;
    }

    private boolean runRankCalculation(String inputPath, String outputPath, Integer runs) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();
        conf.set("TotalPages", totalNumberOfPages.toString());
        conf.set("Iteration", runs.toString());

        Job rankCalculator = Job.getInstance(conf, "rankCalculator");

        rankCalculator.setJarByClass(WikiPageRank.class);

        FileInputFormat.addInputPath(rankCalculator, new Path(inputPath));
        FileOutputFormat.setOutputPath(rankCalculator, deleteIfExistsAndGetPath(outputPath));

        rankCalculator.setInputFormatClass(TextInputFormat.class);
        rankCalculator.setOutputFormatClass(TextOutputFormat.class);

        rankCalculator.setMapperClass(RankCalculatingMapper.class);
        rankCalculator.setMapOutputKeyClass(Text.class);

        rankCalculator.setCombinerClass(RankCalculatingCombiner.class);
        rankCalculator.setReducerClass(RankCalculatingReducer.class);

        rankCalculator.setOutputKeyClass(Text.class);
        rankCalculator.setOutputValueClass(Text.class);

        return rankCalculator.waitForCompletion(true);
    }

    private boolean runIdealRankSorting(String inputPath, String outputPath) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();

        Job idealRankSorter = Job.getInstance(conf, "idealRankSorter");
        idealRankSorter.setJarByClass(WikiPageRank.class);

        FileInputFormat.addInputPath(idealRankSorter, new Path(inputPath));
        FileOutputFormat.setOutputPath(idealRankSorter, deleteIfExistsAndGetPath(outputPath + "ideal"));

        idealRankSorter.setInputFormatClass(TextInputFormat.class);
        idealRankSorter.setOutputFormatClass(TextOutputFormat.class);

        idealRankSorter.setMapperClass(IdealRankSortingMapper.class);

        idealRankSorter.setMapOutputKeyClass(FloatWritable.class);
        idealRankSorter.setMapOutputValueClass(Text.class);

        idealRankSorter.setSortComparatorClass(RankSortingComparator.class);

        idealRankSorter.setReducerClass(RankFormatReducer.class);

        idealRankSorter.setOutputKeyClass(Text.class);
        idealRankSorter.setOutputValueClass(FloatWritable.class);

        return idealRankSorter.waitForCompletion(true);
    }

    private boolean runTaxedRankSorting(String inputPath, String outputPath) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();

        Job taxedRankSorter = Job.getInstance(conf, "taxedRankSorter");
        taxedRankSorter.setJarByClass(WikiPageRank.class);

        FileInputFormat.addInputPath(taxedRankSorter, new Path(inputPath));
        FileOutputFormat.setOutputPath(taxedRankSorter, deleteIfExistsAndGetPath(outputPath + "taxed"));

        taxedRankSorter.setInputFormatClass(TextInputFormat.class);
        taxedRankSorter.setOutputFormatClass(TextOutputFormat.class);

        taxedRankSorter.setMapperClass(TaxedRankSortingMapper.class);

        taxedRankSorter.setMapOutputKeyClass(FloatWritable.class);
        taxedRankSorter.setMapOutputValueClass(Text.class);

        taxedRankSorter.setSortComparatorClass(RankSortingComparator.class);

        taxedRankSorter.setReducerClass(RankFormatReducer.class);

        taxedRankSorter.setOutputKeyClass(Text.class);
        taxedRankSorter.setOutputValueClass(FloatWritable.class);

        return taxedRankSorter.waitForCompletion(true);
    }

    private boolean runDifferenceCalculation(String inputPath, String outputPath) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();

        Job differenceCalculator = Job.getInstance(conf, "differenceCalculator");
        differenceCalculator.setJarByClass(WikiPageRank.class);

        FileInputFormat.addInputPath(differenceCalculator, new Path(inputPath));
        FileOutputFormat.setOutputPath(differenceCalculator, deleteIfExistsAndGetPath(outputPath + "difference"));

        differenceCalculator.setInputFormatClass(TextInputFormat.class);
        differenceCalculator.setOutputFormatClass(TextOutputFormat.class);

        differenceCalculator.setMapperClass(DifferenceCalculatingMapper.class);

        differenceCalculator.setMapOutputKeyClass(Text.class);
        differenceCalculator.setMapOutputValueClass(Text.class);

        differenceCalculator.setCombinerClass(DifferenceAverageReducer.class);
        differenceCalculator.setReducerClass(DifferenceAverageReducer.class);

        differenceCalculator.setOutputKeyClass(Text.class);
        differenceCalculator.setOutputValueClass(Text.class);

        return differenceCalculator.waitForCompletion(true);
    }
}
