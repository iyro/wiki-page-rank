package cs535.pagerank.reducers;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class DifferenceAverageReducer extends Reducer<Text, Text, Text, Text> {
    @Override
    public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        Long globalCount = 0l;
        Float sumOfDifferences = 0.0f;

        for (Text value : values) {
            int indexOfPipeAfterCount = value.find("|", 0);

            Long localCount = Long.parseLong(Text.decode(value.getBytes(), 0, indexOfPipeAfterCount).trim());
            Float localAverage = Float.parseFloat(Text.decode(value.getBytes(), indexOfPipeAfterCount + 1, value.getLength() - indexOfPipeAfterCount - 1).trim());

            sumOfDifferences += (localAverage * localCount);
            globalCount += localCount;
        }

        Float average = sumOfDifferences / globalCount;

        context.write(new Text("Total Count | Average Difference : "), new Text(globalCount.toString() + " | " + average.toString()));
    }
}
