package cs535.pagerank.reducers;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class RankCalculatingCombiner extends Reducer<Text, Text, Text, Text> {
    @Override
    public void reduce(Text page, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        String[] idealAndTaxedRanks;
        Float sumIdealRankShare = 0f;
        Float sumTaxedRankShare = 0f;
        String dataString;

        for (Text value : values) {
            dataString = value.toString();

            if (dataString.equals("*")) {
                context.write(page, new Text("*"));
                continue;
            }

            if (dataString.startsWith("|")) {
                context.write(page, value);
                continue;
            }

            idealAndTaxedRanks = dataString.split("\\t");

            Float idealRankShare = Float.parseFloat(idealAndTaxedRanks[0]);
            Float taxedRankShare = Float.parseFloat(idealAndTaxedRanks[1]);

            sumIdealRankShare += idealRankShare;
            sumTaxedRankShare += taxedRankShare;
        }

        context.write(page, new Text(sumIdealRankShare + "\t" + sumTaxedRankShare));
    }
}