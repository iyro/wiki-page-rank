package cs535.pagerank.reducers;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;


public class PageParsingReducer extends Reducer<Text, Text, Text, Text> {

    @Override
    public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        Float initialIdealRank = 1.0f;
        Float initialTaxedRank = 1.0f;


        String outgoingLinks = "";

        boolean first = true;

        for (Text value : values) {
            if (!first) {
                outgoingLinks += ",";
            }

            outgoingLinks += value.toString();
            first = false;
        }
        context.write(key, new Text(initialIdealRank.toString() + "\t" + initialTaxedRank.toString() + "\t" + outgoingLinks));
    }
}
