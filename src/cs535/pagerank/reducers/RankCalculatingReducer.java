package cs535.pagerank.reducers;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class RankCalculatingReducer extends Reducer<Text, Text, Text, Text> {

    private static final float taxationFactor = 0.85f;

    @Override
    public void reduce(Text page, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        boolean pageExists = false;
        String[] titleAndRanks;
        Float sumIdealRankShare = 0f;
        Float sumTaxedRankShare = 0f;
        String links = "";
        String dataString;

        for (Text value : values) {
            dataString = value.toString();

            if (dataString.equals("*")) {
                pageExists = true;
                continue;
            }

            if (dataString.startsWith("|")) {
                String[] rankAndLinks = dataString.substring(1).split("\\t");
                links = rankAndLinks[2];
                continue;
            }

            titleAndRanks = dataString.split("\\t");

            Float idealRankShare = Float.parseFloat(titleAndRanks[0]);
            Float taxedRankShare = Float.parseFloat(titleAndRanks[1]);

            sumIdealRankShare += idealRankShare;
            sumTaxedRankShare += taxedRankShare;
        }

        if (pageExists) {
            Long totalNumberOfPages;
            totalNumberOfPages = Long.parseLong(context.getConfiguration().get("TotalPages"));
            Float newTaxedRank = (taxationFactor * sumTaxedRankShare) + ((1 - taxationFactor) / totalNumberOfPages);
            context.write(page, new Text(sumIdealRankShare + "\t" + newTaxedRank + "\t" + links));
        }
    }
}