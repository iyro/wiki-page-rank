package cs535.pagerank.reducers;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class RankFormatReducer extends Reducer<FloatWritable, Text, Text, FloatWritable> {

    @Override
    public void reduce(FloatWritable rank, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        for (Text value : values) {
            context.write(value, rank);
        }
    }
}
