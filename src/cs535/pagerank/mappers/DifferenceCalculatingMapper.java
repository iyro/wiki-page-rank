package cs535.pagerank.mappers;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.nio.charset.CharacterCodingException;

/**
 * Created by iyro on 9/30/15.
 */
public class DifferenceCalculatingMapper extends Mapper<LongWritable, Text, Text, Text> {

    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] titleAndRanks = geTitleAndRanks(value);

        Float taxedRank = Float.parseFloat(titleAndRanks[2]);
        Float idealRank = Float.parseFloat(titleAndRanks[1]);

        Float difference = taxedRank - idealRank;
        context.write(new Text("Total Count | Average Difference : "), new Text(Long.toString(1l) + " | " + difference.toString()));
    }

    private String[] geTitleAndRanks(Text value) throws CharacterCodingException {
        String[] pageAndRanks = new String[3];
        int tabAfterPageIndex = value.find("\t");
        int tabAfterIdealRankIndex = value.find("\t", tabAfterPageIndex + 1);
        int tabAfterTaxedRankIndex = value.find("\t", tabAfterIdealRankIndex + 1);

        pageAndRanks[0] = Text.decode(value.getBytes(), 0, tabAfterPageIndex);
        pageAndRanks[1] = Text.decode(value.getBytes(), tabAfterPageIndex + 1, tabAfterIdealRankIndex - tabAfterPageIndex - 1);
        pageAndRanks[2] = Text.decode(value.getBytes(), tabAfterIdealRankIndex + 1, tabAfterTaxedRankIndex - tabAfterIdealRankIndex - 1);

        return pageAndRanks;
    }
}
