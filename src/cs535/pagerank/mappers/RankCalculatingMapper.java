package cs535.pagerank.mappers;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class RankCalculatingMapper extends Mapper<LongWritable, Text, Text, Text> {

    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        int indexOfTabAfterPageTitle = value.find("\t", 0);
        int indexOfTabAfterIdealRank = value.find("\t", indexOfTabAfterPageTitle + 1);
        int indexOfTabAfterTaxedRank = value.find("\t", indexOfTabAfterIdealRank + 1);

        String title = Text.decode(value.getBytes(), 0, indexOfTabAfterPageTitle);
        Float idealRank = Float.parseFloat(Text.decode(value.getBytes(), indexOfTabAfterPageTitle + 1, indexOfTabAfterIdealRank - indexOfTabAfterPageTitle - 1));
        Float taxedRank = Float.parseFloat(Text.decode(value.getBytes(), indexOfTabAfterIdealRank + 1, indexOfTabAfterTaxedRank - indexOfTabAfterIdealRank - 1));

        if (Integer.parseInt(context.getConfiguration().get("Iteration")) == 0) {
            Long totalNumberOfPages;
            totalNumberOfPages = Long.parseLong(context.getConfiguration().get("TotalPages"));
            idealRank = (1.0f / totalNumberOfPages);
            taxedRank = (1.0f / totalNumberOfPages);
        }

        String commaSeparatedOutgoingLinks = Text.decode(value.getBytes(), indexOfTabAfterTaxedRank + 1, value.getLength() - indexOfTabAfterTaxedRank - 1);
        String[] outgoingLinks = commaSeparatedOutgoingLinks.split(",");

        int totalOutgoingLinks = outgoingLinks.length;

        context.write(new Text(title), new Text("*"));  //* page exists
        if (outgoingLinks.length > 0) {
            Float idealRankShare = idealRank / totalOutgoingLinks;
            Float taxedRankShare = taxedRank / totalOutgoingLinks;

            for (String outLink : outgoingLinks) {
                context.write(new Text(outLink), new Text(idealRankShare + "\t" + taxedRankShare));
            }
            context.write(new Text(title), new Text("|" + idealRank + "\t" + taxedRank + "\t" + commaSeparatedOutgoingLinks));
        }
    }
}
