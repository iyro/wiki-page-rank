package cs535.pagerank.mappers;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.nio.charset.CharacterCodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class PageParsingMapper extends Mapper<LongWritable, Text, Text, Text> {

    private static final Pattern LinkRegex = Pattern.compile("\\[\\[.+?\\]\\]");

    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        String[] titleAndContentPair = getTitleAndContentPair(value);

        //title = titleAndContentPair[0]
        //content = titleAndContentPair[1]

        titleAndContentPair[0] = cleanup(titleAndContentPair[0]);
        if (titleAndContentPair[0] == null || titleAndContentPair[0].isEmpty()) return;

        Matcher matcher = LinkRegex.matcher(titleAndContentPair[1]);
        while (matcher.find()) {
            String outLink = matcher.group();
            outLink = getTitleFromLink(outLink);
            outLink = cleanup(outLink);
            if (outLink == null || outLink.isEmpty())
                continue;
            context.write(new Text(titleAndContentPair[0]), new Text(outLink));
        }
    }

    private String cleanup(String title) {
        if (isTitleInvalid(title)) return null;

        title = title.replaceAll("\\s", "_");
        title = title.replace(",", "");
        title = title.replace("&amp;", "&");
        title = title.replace("&quot;", "\"");

        return title;
    }

    private boolean isTitleInvalid(String title) {
        return (title.length() <= 0
                || title.contains(":")
                || title.charAt(0) == '#'
                || title.charAt(0) == ','
                || title.charAt(0) == '.'
                || title.charAt(0) == '&'
                || title.charAt(0) == '\''
                || title.charAt(0) == '-'
                || title.charAt(0) == '{');
    }

    private String getTitleFromLink(String outLink) {
        int start = 2;
        int endLink = outLink.indexOf("]]");

        int pipePosition = outLink.indexOf("|");
        if (pipePosition > 0) {
            endLink = pipePosition;
        }

        int part = outLink.indexOf("#");
        if (part > 0) {
            endLink = part;
        }

        outLink = outLink.substring(start, endLink);

        return outLink;
    }

    private String[] getTitleAndContentPair(Text value) throws CharacterCodingException {
        String[] titleAndText = new String[2];

        int start = value.find("<title>");
        int end = value.find("</title>", start);
        start += 7; //add length of <title>.

        titleAndText[0] = Text.decode(value.getBytes(), start, end - start);

        start = value.find("<text");
        start = value.find(">", start);
        start += 1;
        end = value.find("</text>", start);

        if (start == -1 || end == -1) {
            return new String[]{"", ""};
        }

        titleAndText[1] = Text.decode(value.getBytes(), start, end - start);

        return titleAndText;
    }
}
